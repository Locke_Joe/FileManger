# 办公小助手

软件介绍：本项目用于统一管理日常常用文件，便于打开电脑后可以方便快捷打开常用文件和软件。
有点类似于快捷方式，但是比快捷方式多了可以自定义文件名的部分。
且常文件和软件的存放不受限于在同一个地方。

注意事项：
1. 需要在代码根目录创建文件fileManger.txt
2. 文件里的内容为：“序号 自定义文件名 文件所在本地绝对路径。”如：1 Kettle D:\software\kettle\Spoon.bat
3. 运行后的界面如下图：
![运行后界面.png](https://raw.gitcode.com/Locke_Joe/FileManger/attachment/uploads/a1f5d53e-b160-4175-a977-02b49ac08734/运行后界面.png '运行后界面.png')